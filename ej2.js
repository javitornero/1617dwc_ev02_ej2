//////////////////////////////////////////////////////////////////////////////////////////
// Definimos el código para procesar la respuesta que pasaremos como parametro al objeto
function procesaRespuesta(objeto) {
    var cadena="";
    $("#capaSalida").html("");
    for (i=0;i<objeto.length;i++){
        cadena+="<li>"+objeto[i].nombre+" - "+objeto[i].puntos+" PTS - "+objeto[i].golesAFavor+" goles."+"</li>";
    }    
    $("#capaSalida").html("<ul>"+cadena+"</ul>");
}

function ordena(objeto, orden){ // Función que se encarga de ordenar los objetos
    if (orden=="porPuntos") {
        objeto.sort(function (a, b){
            return (b.puntos - a.puntos)
        })
    }
    if (orden=="porGoles") {
        objeto.sort(function (a, b){
            return (b.golesAFavor - a.golesAFavor)
        })
    }
    return objeto;
}

function ordenaLista(result){
    var objeto=JSON.parse(result);
    var orden=$("#selectOrden").val();
    objeto=ordena(objeto, orden);
    procesaRespuesta(objeto);
}
var listaOriginal="[{}]";
function obtenerDatos(){
    var url = "http://hispabyte.net/DWEC/entregable2-2.php";
    $.ajax({
        url: url,
        method: 'GET',
    }).done(function(result) {
        console.log(result);
        ordenaLista(result);
        listaOriginal=result;
    }).fail(function(err) {
        throw err;
    });
}

// Al carga la pagina asociamos al onclick del boton el método
//window.onload = function() {
$(document).ready(function(){
    $("#pedir").click(obtenerDatos);
    $("#selectOrden").change(function() {
        $("#selectOrden").change(ordenaLista(listaOriginal));
    });
})
